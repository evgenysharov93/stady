package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@SpringBootApplication
@RestController
public class DemoApplication {

    @GetMapping("/hello")
    String home1() {
        return "Hello!!! " + LocalDate.now();
    }

    @GetMapping("/")
    String home() {
        return "Hello wold: now " + LocalDate.now();
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}